using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    private Text ui;
    private static int _scoreTotal = 0;
    
    private void Start()
    {
        _scoreTotal = 0;
        ui = GetComponent<Text>();
    }

    /// <summary>
    /// Allows the word "Score" to be written and its respective score.
    /// </summary>
    private void Update()
    {
        ui.text = "Score: " + _scoreTotal.ToString();
    }

    /// <summary>
    /// Adds 10 to your score value every time you kill an enemy from the ground.
    /// </summary>
    public static void ScoreEnemyGround()
    {
        _scoreTotal += 10;
    }
    
    /// <summary>
    /// Adds 10 to the score value every time you kill a flying enemy.
    /// </summary>
    public static void ScoreEnemyFly()
    {
        _scoreTotal += 15;
    }
}
