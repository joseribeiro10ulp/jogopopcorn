using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Animator))]

public class CharacterMovement : MonoBehaviour
{
    private PlayerHealth healthActual;
    private PauseControl gameOverTime;

    [SerializeField]
    private float speedPlayer = 1.0f;
    [SerializeField]
    private Vector2 velocityPlayer = new Vector2(0, 0);
    [SerializeField]
    private float jumpForce = 13;
    [SerializeField]
    private float jumpDelay = 0.5f;
    [SerializeField]
    private float damageForce = 10;
    
    [SerializeField]
    private Animator animatorPlayer;
    
    public CharacterMovement player;

    [SerializeField] 
    private GameObject gameOver;

    private Rigidbody2D rig;
    private SpriteRenderer sprite; 
    
    private bool isJumping;
    private bool canTriggerJump = false;

    private void Awake()
    {
        animatorPlayer = GetComponent<Animator>();
        this.sprite = this.GetComponent<SpriteRenderer>();
    }
    
    private void Start()
    {
        this.canTriggerJump = true;
        rig = GetComponent<Rigidbody2D>();
        player = gameObject.transform.GetComponent<CharacterMovement>();        
        healthActual = FindObjectOfType<PlayerHealth>();
        gameOverTime = FindObjectOfType<PauseControl>();
        gameOverTime.gameOver = false;
    }
    
    private void Update()
    {        
        Movement();
        Jump();
        LoseLife();
    }
    
    /// <summary>
    /// Allows detection if the jump button is pressed and activates the jump animation.
    /// </summary>
    private void Jump()
    {
        if (Input.GetKeyUp(KeyCode.Space) && !isJumping)
        {
            animatorPlayer.SetBool("isJumping", true);
            if (this.canTriggerJump)
            {
                StartCoroutine(TriggerJump());
            }
        }
        else
        {
            animatorPlayer.SetBool("isJumping", false);
        }
    }

    /// <summary>
    /// Coroutine allows only one jump to be made in 0.5 seconds at a time.
    /// </summary>
    private IEnumerator TriggerJump()
    {
        this.canTriggerJump = false;
        yield return new WaitForSeconds(jumpDelay);
        rig.AddForce(new Vector2(0f, jumpForce), ForceMode2D.Impulse);
        this.canTriggerJump = true;
    }

    /// <summary>
    /// It allows the player to move horizontally, it also makes it rotate when it starts walking backwards. Adds the walking animation as well.
    /// </summary>
    private void Movement()
    {
        float inputAxis = Input.GetAxisRaw("Horizontal");

        if (inputAxis != 0)
        {
            this.velocityPlayer.x = inputAxis * speedPlayer;
            
            animatorPlayer.SetBool("Andar", true);            
        }
        else
        {
            this.velocityPlayer.x = inputAxis * speedPlayer;
        }
        
        if (inputAxis != 0)
        {           
            this.sprite.flipX = this.velocityPlayer.x < 0;
        }
        
        if (inputAxis==0)
        {
            animatorPlayer.SetBool("Andar", false);
        }
        
        Vector3 position = transform.localPosition;
        position.x += velocityPlayer.x * Time.deltaTime;
        transform.localPosition = position;
    }

    /// <summary>
    /// Detects if the player's health is equal to 0 if it calls the death animation.
    /// </summary>
    private void LoseLife()
    {
        if (healthActual.actualHealth<=0)
        {
            animatorPlayer.SetBool("isDeadPlayer",true);
            StartCoroutine(DeadAnimation());
        }
    }
    
    /// <summary>
    /// It allows the detection of the ground to see if it can jump or not.
    /// If detects an Enemy collision the impulse force will be applied.
    /// Detects if the player hit the enemy, if it calls the function of losing life.
    /// Detects if the player hit the void, if it calls the function to lose all life. 
    /// </summary>
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            player.isJumping = false;                
        }
        
        if (collision.gameObject.CompareTag("Enemy"))
        {            
            var force = transform.position - collision.transform.position;
            force.Normalize();
            gameObject.GetComponent<Rigidbody2D>().AddForce(force * damageForce, ForceMode2D.Impulse);
        }

        if (collision.gameObject.CompareTag("Void"))
        {
            healthActual.TakeDamageVoid();
        }
        
        if (collision.gameObject.CompareTag("Enemy"))
        {
            healthActual.TakeDamage();            
        }
    }

    /// <summary>
    /// It allows the detection of the ground to see if it can jump or not.
    /// </summary>
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            player.isJumping = true;          
        }
    }

    /// <summary>
    /// Coroutine for the death animation to be displayed. In addition displays the game over panel.
    /// </summary>
    private IEnumerator DeadAnimation()
    {
        yield return new WaitForSeconds(1f);
        gameOver.SetActive(true);
        gameOverTime.gameOver = true;
    }
}
