using UnityEngine;

public class RaiseWall : MonoBehaviour
{
    /// <summary>
    /// Disable the door.
    /// </summary>
    public void Door()
    {
        gameObject.SetActive(false);
    }
}
