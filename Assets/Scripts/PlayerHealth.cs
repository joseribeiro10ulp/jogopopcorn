using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
    public int maxHealth;
    public float actualHealth;
    
    private void Start()
    {
        actualHealth = maxHealth;        
    }

    private void Update()
    {
        if (actualHealth <= 0)
        {
            Debug.Log("Game Over");
        }
    }

    /// <summary>
    /// Deals 20 damage when it touches the enemy.
    /// </summary>
    public void TakeDamage()
    {
        actualHealth -=20;
    }

    /// <summary>
    /// Deals 0.02 damage per frame when the raycast touches the player.
    /// </summary>
    public void TakeDamageEnemyFly()
    {
        actualHealth -=0.02f;
    }

    /// <summary>
    /// Lose your whole life if you fall into the void.
    /// </summary>
    public void TakeDamageVoid()
    {
        actualHealth -= maxHealth;
    }
}
