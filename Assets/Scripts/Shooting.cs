using System.Collections;
using UnityEngine;

public class Shooting : MonoBehaviour
{   
    [SerializeField]
    private GameObject bullet;
    [SerializeField]
    private Transform firePoint;
    [SerializeField]
    private Transform launchBullet;
    [SerializeField]
    private float bulletSpeed = 50;

    private Vector2 lookDirection;
    private GameObject bulletClone;
    private float lookAngle;    
    
    private void Update()
    {
        MouseDirection();
        PressToShoot();
    }
    
    /// <summary>
    /// Allows the weapon to orient itself in the position the mouse is in.
    /// </summary>
    private void MouseDirection()
    {
        lookDirection = Camera.main.ScreenToWorldPoint(Input.mousePosition) - new Vector3(firePoint.transform.position.x, firePoint.transform.position.y);
        lookAngle = Mathf.Atan2(lookDirection.y, lookDirection.x) * Mathf.Rad2Deg;
        lookAngle -= 90;
        firePoint.rotation = Quaternion.Euler(0, 0, lookAngle);
    }

    /// <summary>
    /// Allows firing, creating a new bullet every time it is fired, it is still called the coroutine which causes the bullet to disappear if it does not hit anything.
    /// </summary>
    private void PressToShoot()
    {
        if (Input.GetMouseButtonDown(0))
        {
            bulletClone = Instantiate(bullet);
            bulletClone.transform.position = launchBullet.position;
            bulletClone.transform.rotation = Quaternion.Euler(0, 0, lookAngle);
            bulletClone.GetComponent<Rigidbody2D>().velocity = launchBullet.right * bulletSpeed;
            StartCoroutine(Bullet(bulletClone));
        }
    }

    /// <summary>
    /// Coroutine to make the bullet disappear.
    /// </summary>
    /// <param name="bullet"></param>
    /// <returns></returns>
    private static IEnumerator Bullet(GameObject bullet)
    {        
        yield return new WaitForSeconds(2f);
        Destroy(bullet);
    }
}
