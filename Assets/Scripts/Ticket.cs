using UnityEngine;

public class Ticket : MonoBehaviour
{
   [SerializeField]
   private int ticketValue = 1;
   
   public TicketVerification ticketVerification;

   /// <summary>
   /// When the player collides with the ticket, it makes the screen add one more in the tickets sector,
   /// and destroys the ticket itself.
   /// </summary>
   /// <param name="hit"></param>
   private void OnTriggerEnter2D(Collider2D hit)
   {
      if (hit.CompareTag("Player"))
      {
         TicketManager.instance.ChangeTicket(ticketValue);
         ticketVerification.SumTicket();
         Destroy(gameObject);
      }
   }
}
