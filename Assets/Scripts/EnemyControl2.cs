using UnityEngine;

public class EnemyControl2 : MonoBehaviour
{
    private PlayerHealth healthActual;
    
    [SerializeField]
    private Transform castPoint;
    [SerializeField]
    private float checkRange;
    [SerializeField]
    private LineRenderer lineRenderer;
    [SerializeField]
    public SpriteRenderer spriteRenderer;

    private void Start()
    {
        lineRenderer = GetComponentInChildren<LineRenderer>();
        healthActual = FindObjectOfType<PlayerHealth>();
    }

    private void Update()
    {
        CanSeeThePlayer(checkRange);
    }
    
    /// <summary>
    /// Creation of the raycast, detecting the player. DrawLine is used to know in the scene if the raycast is working.
    /// If hits the player it activates the LineRender component, if it does not disable it.
    /// </summary>
    /// <param name="distance"></param>
    private void CanSeeThePlayer(float distance)
    {
        float cast = distance;
        Vector2 endposition = castPoint.position + Vector3.down* cast;
        RaycastHit2D hit = Physics2D.Linecast(castPoint.position, endposition, 1 << LayerMask.NameToLayer("PlaceGame"));
        lineRenderer.enabled = false;
        
        if(hit.collider != null )
        {
            if(hit.collider.gameObject.CompareTag("Player"))
            {
                lineRenderer.enabled = true;
                Draw2D(castPoint.position, hit.point);
                spriteRenderer.color = Color.red;
                healthActual.TakeDamageEnemyFly();
            }
            Debug.DrawLine(castPoint.position, hit.point, Color.red);
        }
        else
        {
            lineRenderer.enabled = false;
            spriteRenderer.color = Color.white;
            Debug.DrawLine(castPoint.position, endposition, Color.green);
            spriteRenderer.color = Color.white;
        }
    }
    
    /// <summary>
    /// Draws the LineRender.
    /// </summary>
    private void Draw2D(Vector2 startPos, Vector2 endPos)
    {
        lineRenderer.SetPosition(0,startPos);
        lineRenderer.SetPosition(1,endPos);
    }
    
    /// <summary>
    /// Disables the enemy as soon as it is shot.
    /// </summary>
    private void OnTriggerEnter2D(Collider2D hit)
    {
        if (hit.CompareTag("Shot"))
        {
            gameObject.SetActive(false);
        }      
    }
}
