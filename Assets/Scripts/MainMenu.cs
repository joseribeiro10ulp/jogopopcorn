using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    /// <summary>
    /// Calls the game scene.
    /// </summary>
   public void PlayGame()
   { 
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
   }

    /// <summary>
    /// Exit the game. Only works in build.
    /// </summary>
   public void QuitGame()
   {
        Application.Quit();
   }
}
