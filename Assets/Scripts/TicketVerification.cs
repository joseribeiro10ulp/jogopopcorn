using UnityEngine;
using UnityEngine.Events;

public class TicketVerification : MonoBehaviour
{
    public UnityEvent raiseWall;

    [SerializeField]
    private int totalTicket=3;
    
    public int pickTicket;
    private bool allPicked;
    
    /// <summary>
    /// Check if the amount of tickets collected is equal to the number of tickets needed to open the door.
    /// If so, call unitEvents "RaiseWall".
    /// </summary>
    private void Update()
    {
        if (pickTicket == totalTicket)
        {
            allPicked = true;
        }

        if (allPicked)
        {
            raiseWall.Invoke();
        }
    }

    /// <summary>
    /// Add one more ticket to the amount of tickets collected.
    /// </summary>
    public void SumTicket()
    {
        pickTicket++;
    }
}
