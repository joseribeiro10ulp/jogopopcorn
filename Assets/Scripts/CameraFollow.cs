﻿using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    [SerializeField]
    private Transform player;
    [SerializeField]
    private Vector3 offset;

    /// <summary>
    /// Causes the camera to follow the player more smoothly.
    /// </summary>
    private void FixedUpdate()
    {
        var position = player.position;
        transform.position = new Vector3(position.x + offset.x, position.y + offset.y, offset.z);
    }
}