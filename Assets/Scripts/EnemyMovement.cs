using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    [SerializeField] 
    private Transform[] positions;
    [SerializeField] 
    private float speedMovement;

    private int nextWaypointIndex;
    private Transform nextWaypoint;

    private void Start()
    {
        nextWaypoint = positions[0];
    }

    private void Update()
    {
        MoveEnemy();
    }

   /// <summary>
   /// It allows to move the transform of the gameObject, through the positions of the waypoints found in the array.
   /// </summary>
    private void MoveEnemy()
    {
        if (transform.position == nextWaypoint.position)
        {
            nextWaypointIndex++;
            if (nextWaypointIndex >= positions.Length)
            {
                nextWaypointIndex = 0;
            }
            nextWaypoint = positions[nextWaypointIndex];
        }
        else
        {
            transform.position = Vector3.MoveTowards(transform.position, nextWaypoint.position, speedMovement * Time.deltaTime);
        }
    }
}
