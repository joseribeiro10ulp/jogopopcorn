using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public static class SaveSystem
{
    /// <summary>
    /// Transforms the coordinate values into a binary format and store this information in a file with the ending /player.info.
    /// </summary>
    public static void SavePlayer(CharacterMovement player)
    {
        BinaryFormatter formatter = new BinaryFormatter();
        
        string path = Application.persistentDataPath + "/player.info";
        FileStream stream = new FileStream(path, FileMode.Create);

        PlayerData data = new PlayerData(player);
        
        formatter.Serialize(stream,data);
        stream.Close();
    }

    /// <summary>
    /// It will get the values saved in the file created in the save method, decoding that file.
    /// </summary>
    public static PlayerData LoadPlayer()
    {
        string path = Application.persistentDataPath + "/player.info";
        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);
            
            PlayerData data=formatter.Deserialize(stream) as PlayerData;
            stream.Close();

            return data;
        }
        else
        {
            Debug.Log("Save file not found" + path);
            return null;
        }
    }
}
