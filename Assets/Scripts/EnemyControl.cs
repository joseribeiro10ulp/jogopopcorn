using System.Collections;
using UnityEngine;

public class EnemyControl : MonoBehaviour
{
    private PlayerHealth healthActual;

    [SerializeField]
    private Transform player;
    [SerializeField]
    private SpriteRenderer spriteRenderer;
    [SerializeField]
    private float checkRange;
    [SerializeField]
    private float moveSpeed;
    
    private Rigidbody2D rb2d;
    private Animator animator;
    
    private bool isFacingLeft;
    private bool isRange;
    private bool isSearching;

    private void Start()
    {
        healthActual = FindObjectOfType<PlayerHealth>();
        rb2d=GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

    private void Update()
    {
        Attack();
    }

    /// <summary>
    /// Method that checks whether the enemy can see the player or not, performing an attack if it is at a distance that allows them to look.
    /// Continue searching for player for 2s even if not in the distance.
    /// </summary>
    private void Attack()
    {
        if (CanSeePlayer(checkRange))
        {
            isRange = true;
        }
        else
        {
            if (isRange)
            {
                if (!isSearching)
                {
                    isSearching = true;
                    Invoke(nameof(StopChasingPlayer), 2f);
                }
            }
        }
        if (isRange)
        {
            ChasePlayer();
        }
    }
    
    /// <summary>
    /// Creation of the raycast, detecting the player. DrawLine is used to know in the scene if the raycast is working.
    /// </summary>
    /// <param name="distance"></param>
    /// <returns></returns>
    private bool CanSeePlayer(float distance)
    {
        bool val = false;
        float castDist = distance;

        if (isFacingLeft)
        {
            castDist = -distance;
        }
        
        Vector2 endPos = transform.position + Vector3.right * castDist;
        RaycastHit2D hit = Physics2D.CircleCast(transform.position, checkRange,Vector2.zero, checkRange,1 << LayerMask.NameToLayer("PlaceGame"));
        animator.SetBool("isAtacking",false);
        
        if (hit.collider != null)
        {
            if (hit.collider.gameObject.CompareTag("Player"))
            {
                val = true;            
            }
            else
            {
                val = false;
            }
            Debug.DrawLine(transform.position, hit.point, Color.red);
        }
        else
        {
            Debug.DrawLine(transform.position, endPos, Color.green);
        }
        return val;
    }
    
    /// <summary>
    /// Allows the enemy to chase the player, both in the positive and negative direction of the x-axis.
    /// </summary>
    private void ChasePlayer()
    {
        if(transform.position.x < player.position.x)
        {
            rb2d.velocity = new Vector2(moveSpeed, 0);
            transform.localScale = new Vector2(1, 1);
            isFacingLeft = false;
            animator.SetBool("isAtacking",true);
        }
        else
        {
            rb2d.velocity = new Vector2(-moveSpeed, 0);
            transform.localScale = new Vector2(-1, 1);
            isFacingLeft = true;
            animator.SetBool("isAtacking",true);
        }
    }
    
    /// <summary>
    /// Allows the enemy to stop chasing the player.
    /// </summary>
    private void StopChasingPlayer()
    {
        isRange = false;
        isSearching = false;
        rb2d.velocity = new Vector2(0, 0);
        animator.SetBool("isAtacking",false);
    }

    /// <summary>
    /// Allows the enemy to be destroyed after being hit by a bullet.
    /// </summary>
    /// <param name="hit"></param>
    private void OnTriggerEnter2D(Collider2D hit)
    {
        if (hit.CompareTag("Shot"))
        {
            StartCoroutine(WaitForDie());
            animator.SetBool("isDead",true);
        }      
    }

    /// <summary>
    /// Coroutine to allow time for animation to happen.
    /// </summary>
    private IEnumerator WaitForDie()
    {
        isRange = false;
        isSearching = false;
        yield return new WaitForSeconds(0.5f);
        gameObject.SetActive(false);
    }
}
