[System.Serializable]
public class PlayerData
{
    public float[] position;

    /// <summary>
    /// Method that retrieves the player's transforms and transforms their coordinates into an array.
    /// </summary>
    public PlayerData(CharacterMovement playerController)
    {
        position = new float[3];
        position[0] = playerController.transform.position.x;
        position[1] = playerController.transform.position.y;
        position[2] = playerController.transform.position.z;
    }
}
