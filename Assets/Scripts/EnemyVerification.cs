using UnityEngine;
using UnityEngine.Events;

public class EnemyVerification : MonoBehaviour
{
    /// <summary>
    /// This script is not used, but it could be in the future for enemy counting.
    /// </summary>
    
    
    public UnityEvent raiseWall;

    [SerializeField]
    private int totalEnemy=3;
    
    private int deadEnemy;
    private bool allDead;
    
    /// <summary>
    /// Checks if the number of dead enemies is equal to the number of dead enemies needed to open the door. If yes, calls unityEvents "RaiseWall".
    /// </summary>
    private void Update()
    {
        if (deadEnemy == totalEnemy)
        {
            allDead = true;
        }

        if (allDead)
        {
            raiseWall.Invoke();
        }
    }
    
    /// <summary>
    /// Counter of dead enemies.
    /// </summary>
    public void SumEnemy()
    {
        deadEnemy++;
    }
}
