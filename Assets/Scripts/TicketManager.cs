using UnityEngine;
using UnityEngine.UI;

public class TicketManager : MonoBehaviour
{
    public static TicketManager instance;
    public Text text;
    
    public int scoreTicket;

    private void Start()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    /// <summary>
    /// Changes the text of tickets.
    /// </summary>
    /// <param name="ticketValue"></param>
    public void ChangeTicket(int ticketValue)
    {
        scoreTicket += ticketValue;
        text.text = scoreTicket.ToString() + "of 6";
    }
}