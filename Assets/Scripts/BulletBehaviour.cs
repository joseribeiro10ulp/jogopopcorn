using UnityEngine;

public class BulletBehaviour : MonoBehaviour
{
    private Score totalScore;
    
    private void Start()
    {
        totalScore = FindObjectOfType<Score>();
    }    

    /// <summary>
    /// Detects if the bullet hits the ground or an enemy, causing it to destroy itself;
    /// If you hit an enemy activates the sum of the score (+10).
    /// If you hit an enemy activates the sum of the score (+15).
    /// </summary>
    private void OnTriggerEnter2D(Collider2D hit)
    {
        if (hit.gameObject.CompareTag("Ground"))
        {
            Destroy(gameObject);
        }
        
        if (hit.gameObject.CompareTag("Enemy"))
        {
            Destroy(gameObject);
            Score.ScoreEnemyGround();
        }
        
        if (hit.gameObject.CompareTag("EnemyFly"))
        {
            Destroy(gameObject);
            Score.ScoreEnemyFly();
        }
    }
}
