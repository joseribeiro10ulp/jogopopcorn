using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseControl : MonoBehaviour
{
    public CharacterMovement position;
   
    [SerializeField] 
    public GameObject pauseMenuUI;
    
    private static bool gamePaused = false;
    public bool gameOver;

    private void Start()
    {
        position = FindObjectOfType<CharacterMovement>();
    }

    private void Update()
    {
      GetKeyPause();
      if (gameOver)
      {
          GameOver();
      }
      
      if (!gameOver && !gamePaused)
      {
          Time.timeScale = 1F;
      }
    }

    /// <summary>
    /// Detects whether the pause button is pressed.
    /// </summary>
    private void GetKeyPause()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (gamePaused)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }
    }
    
    /// <summary>
    /// Activates the pause menu and freezes the time.
    /// </summary>
    private void Pause()
    {
        pauseMenuUI.SetActive((true));
        Time.timeScale = 0F;
        gamePaused = true;
    }
    
    /// <summary>
    /// Return to the game by taking the pause.
    /// </summary>
    public void Resume()
    {
        pauseMenuUI.SetActive((false));
        Time.timeScale = 1F;
        gamePaused = false;
    }

    /// <summary>
    /// Calls the game save method by assigning the new location with the coordinates of the location when the save.
    /// </summary>
    public void LoadGame()
    {
        PlayerData data= SaveSystem.LoadPlayer();

        Vector3 positionnew;
        positionnew.x = data.position[0];
        positionnew.y = data.position[1];
        positionnew.z = data.position[2];
        position.transform.position = positionnew;
        
        Time.timeScale = 1F;
        pauseMenuUI.SetActive(false);
    }

    /// <summary>
    /// Calls the save method in SaveSystem script.
    /// </summary>
    public void SaveGame()
    {
        SaveSystem.SavePlayer(position);
    }

    /// <summary>
    /// Freezes game time.
    /// </summary>
    private static void GameOver()
    {
        Time.timeScale = 0F;
    }
    
    /// <summary>
    /// Restart the game. Calls the game scene when the button is pressed.
    /// </summary>
    public void Restart()
    {
        SceneManager.LoadScene("Game");
    }
    
    /// <summary>
    /// Calls the menu scene.
    /// </summary>
    public void GoMenu()
    {
        Time.timeScale = 1F;
        SceneManager.LoadScene("Menu");
    }
    
    /// <summary>
    /// Exit the game. Only works in build.
    /// </summary>
    public void QuitGame()
    { 
        Debug.Log(("Quit Game"));
        Application.Quit(); 
    }
}
