using System.Collections;
using UnityEngine;

public class PortalBehaviour : MonoBehaviour
{
    [SerializeField]
    private GameObject portal;
    [SerializeField] 
    private GameObject player;
    [SerializeField] 
    private float distance = 0.5f;
    
    /// <summary>
    /// If the player collides with the portal and is at a distance greater than the value of the variable"distance",
    /// starts the coroutine that performs the teleportation.
    /// </summary>
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag(("Player")))
        {
            if (Vector2.Distance(transform.position, other.transform.position) > distance)
            {
                StartCoroutine(Teleport());
            }
        }
    }
    
    /// <summary>
    /// Corouine which equals the player's position with the portal's position thus making the teleport.
    /// </summary>
    private IEnumerator Teleport()
    {
        yield return new WaitForSeconds(0.2f);
        player.transform.position = new Vector2(portal.transform.position.x, portal.transform.position.y);
    }
}
