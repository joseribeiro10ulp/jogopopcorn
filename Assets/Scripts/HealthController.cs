using UnityEngine;
using UnityEngine.UI;

public class HealthController : MonoBehaviour
{
    public PlayerHealth playerHealth;
    public float healthMax;
    public Image healthBar;

    private void Start()
    {
        playerHealth = FindObjectOfType<PlayerHealth>();
    }
    private void Update()
    {
        UpdateHealthBar();
    }
    
    /// <summary>
    /// Allows you to change the amount of life(green) that appears in the UI bar.
    /// </summary>
    private void UpdateHealthBar()
    {
        healthBar.fillAmount = playerHealth.actualHealth / healthMax;
    }
}
